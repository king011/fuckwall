#!/bin/bash
#Program:
#       golang 自動編譯 腳本
#History:
#       2018-03-28 king first release
#		2018-08-30 tag commit date
#Email:
#       zuiwuchang@gmail.com

# 定義的 各種 輔助 函數
MkDir(){
	mkdir -p "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
MkOrClear(){
	if test -d "$1";then
		declare path="$1"
		path+="/*"
		rm "$path" -rf
		if [ "$?" != 0 ] ;then
			exit 1
		fi
	else
		MkDir $1
	fi
}
NewFile(){
	echo "$2" > "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
WriteFile(){
	echo "$2" >> "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}

check(){
	if [ "$1" != 0 ] ;then
		exit $1
	fi
}
CreateGoVersion(){
	MkDir version
	filename="version/version.go"
	package="version"

	# 返回 git 信息 時間
	tag=`git describe`
	if [ "$tag" == '' ];then
		tag="[unknown tag]"
	fi

	commit=`git rev-parse HEAD`
	if [ "$commit" == '' ];then
		commit="[unknow commit]"
	fi
	
	date=`date +'%Y-%m-%d %H:%M:%S'`

	# 打印 信息
	echo ${tag} $commit
	echo $date


	# 自動 創建 go 代碼
	NewFile $filename	"package $package"
	WriteFile $filename	''
	WriteFile $filename	'// Tag git tag'
	WriteFile $filename	"const Tag = \`$tag\`"
	WriteFile $filename	'// Commit git commit'
	WriteFile $filename	"const Commit = \`$commit\`"
	WriteFile $filename	'// Date build datetime'
	WriteFile $filename	"const Date = \`$date\`"
}

function CreateLinux(){
	MkDir cmd/client/sys
	filename="cmd/client/sys/sys.go"
	NewFile $filename	"package sys"
	WriteFile $filename	''
	WriteFile $filename	'/*'
	WriteFile $filename	'#include <memory.h>'
	WriteFile $filename	'#include <arpa/inet.h>'
	WriteFile $filename	'#include <linux/netfilter_ipv4.h>'
	WriteFile $filename	''
	WriteFile $filename	'#ifndef IP6T_SO_ORIGINAL_DST'
	WriteFile $filename	'#define IP6T_SO_ORIGINAL_DST 80'
	WriteFile $filename	'#endif'
	WriteFile $filename	''
	WriteFile $filename	'static int getdestaddr(int fd, struct sockaddr_storage *destaddr)'
	WriteFile $filename	'{'
	WriteFile $filename	'    socklen_t socklen = sizeof(*destaddr);'
	WriteFile $filename	'    int error = getsockopt(fd, SOL_IPV6, IP6T_SO_ORIGINAL_DST, destaddr, &socklen);'
	WriteFile $filename	'    if (error) { // Didn not find a proper way to detect IP version.'
	WriteFile $filename	'        error = getsockopt(fd, SOL_IP, SO_ORIGINAL_DST, destaddr, &socklen);'
	WriteFile $filename	'        if (error) {'
	WriteFile $filename	'            return -1;'
	WriteFile $filename	'        }'
	WriteFile $filename	'    }'
	WriteFile $filename	'    return 0;'
	WriteFile $filename	'}'
	WriteFile $filename	'static int native_getdestaddr(int fd,char* ip,int* n,in_port_t* port)'
	WriteFile $filename	'{'
	WriteFile $filename	'	struct sockaddr_storage addr;'
	WriteFile $filename	'	int error = getdestaddr(fd,&addr);'
	WriteFile $filename	'	if(error){'
	WriteFile $filename	'		return -1;'
	WriteFile $filename	'	}'
	WriteFile $filename	'	if(addr.ss_family == AF_INET6){'
	WriteFile $filename	'		*n = 16;'
	WriteFile $filename	'        struct sockaddr_in6 *addr_v6 = (struct sockaddr_in6 *)&addr;'
	WriteFile $filename	'		memcpy(ip,&(addr_v6->sin6_addr),16);'
	WriteFile $filename	'		*port = ntohs(addr_v6->sin6_port);'
	WriteFile $filename	'    }else{'
	WriteFile $filename	'		*n = 4;'
	WriteFile $filename	'		struct sockaddr_in *addr_v4 = (struct sockaddr_in *)&addr;'
	WriteFile $filename	'		memcpy(ip,&(addr_v4->sin_addr),4);'
	WriteFile $filename	'		*port = ntohs(addr_v4->sin_port);'
	WriteFile $filename	'    }'
	WriteFile $filename	'	return 0;'
	WriteFile $filename	'}'
	WriteFile $filename	'*/'
	WriteFile $filename	'import "C"'
	WriteFile $filename	'import "net"'
	WriteFile $filename	'import "unsafe"'
	WriteFile $filename	''
	WriteFile $filename	'// GetDestAddr .'
	WriteFile $filename	'func GetDestAddr(fd int) *net.TCPAddr {'
	WriteFile $filename	'	var n int'
	WriteFile $filename	'	b := make([]byte, 16)'
	WriteFile $filename	'	var port uint16'
	WriteFile $filename	'	v := C.native_getdestaddr('
	WriteFile $filename	'		C.int(fd),'
	WriteFile $filename	'		(*C.char)(unsafe.Pointer(&b[0])),'
	WriteFile $filename	'		(*C.int)(unsafe.Pointer(&n)),'
	WriteFile $filename	'		(*C.in_port_t)(unsafe.Pointer(&port)),'
	WriteFile $filename	'	)'
	WriteFile $filename	'	if v != 0 {'
	WriteFile $filename	'		return nil'
	WriteFile $filename	'	}'
	WriteFile $filename	'	var ip net.IP'
	WriteFile $filename	'	if v == 4 {'
	WriteFile $filename	'		ip = net.IPv4(b[0], b[1], b[2], b[3])'
	WriteFile $filename	'	} else {'
	WriteFile $filename	'		ip = net.IP(b[:n])'
	WriteFile $filename	'	}'
	WriteFile $filename	'	return &net.TCPAddr{'
	WriteFile $filename	'		IP:   ip,'
	WriteFile $filename	'		Port: int(port),'
	WriteFile $filename	'	}'
	WriteFile $filename	'}'
}

function CreateWindows(){
	MkDir cmd/client/sys
	filename="cmd/client/sys/sys.go"
	NewFile $filename	"package sys"
	WriteFile $filename	''
	WriteFile $filename	'import "net"'
	WriteFile $filename	''
	WriteFile $filename	'// GetDestAddr .'
	WriteFile $filename	'func GetDestAddr(fd int) *net.TCPAddr {'
	WriteFile $filename	'	return nil'
	WriteFile $filename	'}'
}

# 顯示幫助信息
function ShowHelp(){
	echo "help        : show help"
    echo "l/linux     : build for linux"
    echo "w/windows   : build for windows" 
	echo "n/normal    : run go build"
}

case $1 in
	n|normal)
		export GOOS=linux
		CreateLinux
		CreateGoVersion
		go build -ldflags "-s -w" -o bin/fuckwall
	;;
	l|linux)
		export GOOS=linux
		CreateLinux
		CreateGoVersion
		go build -ldflags "-s -w" -o bin/fuckwall
		check $?

		dst=linux.amd64.tar.gz
		if [[ $GOARCH == 386 ]];then
			dst=linux.386.tar.gz
		fi
		tar -zcvf $dst bash_completion/fuckwall.sh	\
			bin/fuckwall bin/fuckwall.service \
			bin/server.jsonnet bin/client.jsonnet \
			bin/server.key bin/server.pem \
			bin/tools/Corefile bin/tools/coredns.service bin/tools/*.sh 
	;;

	w|windows)
		export GOOS=windows
		CreateWindows
		CreateGoVersion
		go build -ldflags "-s -w" -o bin/fuckwall.exe
		check $?

		dst=windows.amd64.tar.gz
		if [[ $GOARCH == 386 ]];then
			dst=windows.386.tar.gz
		fi
		tar -zcvf $dst bash_completion/fuckwall.sh	\
			bin/fuckwall.exe \
			bin/server.jsonnet bin/client.jsonnet
	;;

	*)
		ShowHelp
		exit 0
	;;
esac