// Package tquic 使用 quic 實現 傳輸層
package tquic

import (
	"crypto/tls"
	"net"

	quic "github.com/lucas-clemente/quic-go"
	"gitlab.com/king011/fuckwall/transport"
)

// Transport 傳輸 通道 名稱
const Transport = "quic"

type _Listener struct {
	listener quic.Listener
	password []byte
}

// NewListener 創建監聽 機器
func NewListener(password []byte, addr string,
	tlsConf *tls.Config,
	quicConf *quic.Config) (listener transport.Listener, e error) {
	l, e := quic.ListenAddr(addr, tlsConf, quicConf)
	if e != nil {
		return
	}
	listener = &_Listener{
		listener: l,
		password: password,
	}
	return
}

func (l *_Listener) Close() error {
	return l.listener.Close()
}
func (l *_Listener) Addr() net.Addr {
	return l.listener.Addr()
}
func (l *_Listener) Accept() (transport.Session, error) {
	s, e := l.listener.Accept()
	if e != nil {
		return nil, e
	}
	return &_Session{
		session:  s,
		server:   true,
		password: l.password,
	}, nil
}
