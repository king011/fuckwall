package tquic

import (
	"bytes"
	"context"
	"sync"
	"time"

	"errors"

	quic "github.com/lucas-clemente/quic-go"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
)

var errNotSupportAcceptStream = errors.New("client session not support AcceptStream")
var errNotSupportOpenStream = errors.New("server session not support OpenStream")

type _Stream struct {
	quic.Stream
}

func (_Stream) Transport() string {
	return Transport
}

type _Session struct {
	sync.Mutex
	// quic 連接
	session quic.Session
	// 是否是 服務器 session
	server bool
	// 是否 已經 登入
	login    bool
	password []byte
}

// Transport 返回 傳輸協議 名稱
func (s *_Session) Transport() string {
	return Transport
}
func (s *_Session) Close() (e error) {
	return s.session.Close()
}

func (s *_Session) AcceptStream() (transport.Stream, error) {
	if !s.server {
		return nil, errNotSupportAcceptStream
	}
	stream, e := s.session.AcceptStream()
	if e != nil {
		return nil, e
	}
	s.Lock()
	if s.login {
		s.Unlock()
		// 已經 登入 直接 stream
		return _Stream{stream}, nil
	}
	// 等待 登入
	e = s.waitLogin(stream)
	s.Unlock()
	if e != nil {
		stream.Close()
		return nil, e
	}
	return _Stream{stream}, nil
}
func (s *_Session) waitLogin(stream quic.Stream) (e error) {
	session := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	// wait request
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	request, e := session.Recv(ctx)
	if e != nil {
		return
	}
	cmd := message.Command(request)
	if cmd != transport.CommnadLogin {
		e = transport.ErrPermissionDenied

		// 向 客戶端 返回 錯誤信息
		response, _ := message.Marshal(transport.CommnadError, []byte(e.Error()))
		if response != nil {
			session.Send(ctx, response)
		}
		return
	}
	// 驗證 密碼
	body := message.Body(request)
	if bytes.Equal(s.password, body) {
		// 通知 客戶端 登入 成功
		response, _ := message.Marshal(transport.CommnadSuccess, nil)
		e = session.Send(ctx, response)
		if e != nil {
			return
		}
		s.login = true
	} else {
		e = transport.ErrPasswordNotMatch
		// 向 客戶端 返回 錯誤信息
		response, _ := message.Marshal(transport.CommnadError, []byte(e.Error()))
		if response != nil {
			session.Send(ctx, response)
		}
		return
	}
	return
}

func (s *_Session) OpenStream() (transport.Stream, error) {
	if s.server {
		return nil, errNotSupportOpenStream
	}
	stream, e := s.session.OpenStreamSync()
	if e != nil {
		return nil, e
	}

	s.Lock()
	if s.login {
		s.Unlock()
		// 已經 登入 直接 stream
		return _Stream{stream}, nil
	}
	// 登入
	e = s.doneLogin(stream)
	s.Unlock()
	if e != nil {
		stream.Close()
		return nil, e
	}
	return _Stream{stream}, e
}
func (s *_Session) doneLogin(stream quic.Stream) (e error) {
	session := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	// request
	request, e := message.Marshal(transport.CommnadLogin, s.password)
	if e != nil {
		return
	}
	// call
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	response, e := session.Call(ctx, request)
	cancel()
	if e != nil {
		return
	}
	// response
	e = utils.CheckResponse(response)
	if e != nil {
		return
	}

	// set flag
	s.login = true
	return
}
