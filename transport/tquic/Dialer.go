package tquic

import (
	"crypto/tls"

	quic "github.com/lucas-clemente/quic-go"
	"gitlab.com/king011/fuckwall/transport"
)

type _Dialer struct {
	password []byte
	addr     string
	tlsConf  *tls.Config
	quicConf *quic.Config
}

// NewDialer 創建一個 撥號機器
func NewDialer(password []byte,
	addr string,
	tlsConf *tls.Config,
	quicConf *quic.Config) (dialer transport.Dialer) {
	dialer = &_Dialer{
		password: password,
		addr:     addr,
		tlsConf:  tlsConf,
		quicConf: quicConf,
	}
	return
}

// Repeat 斷線後 需要 手動 重撥
func (d *_Dialer) Repeat() bool {
	return true
}

// Dial 向服務器 撥號
func (d *_Dialer) Dial() (session transport.Session, e error) {
	s, e := quic.DialAddr(d.addr, d.tlsConf, d.quicConf)
	if e != nil {
		return
	}
	session = &_Session{
		session:  s,
		server:   false,
		password: d.password,
	}
	return
}
