package tgrpc

import (
	"crypto/tls"
	"errors"
	"gitlab.com/king011/fuckwall/transport"
	tgrpc_transport "gitlab.com/king011/fuckwall/transport/tgrpc/transport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"time"
)

var errConnMiss = errors.New("grpc conn miss")

type _Dialer struct {
	conn *grpc.ClientConn
}

// NewDialer 創建 grpc 撥號器
func NewDialer(pwd []byte,
	addr string,
	tlsConf *tls.Config,
) (transport.Dialer, error) {
	opts := make([]grpc.DialOption, 0, 10)
	if tlsConf == nil {
		opts = append(opts,
			grpc.WithTransportCredentials(
				tgrpc_transport.NewClientCreds(nil, pwd),
			),
		)
	} else {
		creds := credentials.NewTLS(tlsConf)
		opts = append(opts,
			grpc.WithTransportCredentials(
				tgrpc_transport.NewClientCreds(creds, pwd[:]),
			),
		)
	}
	opts = append(opts,
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                time.Minute,
			Timeout:             time.Second * 10,
			PermitWithoutStream: true,
		}),
	)
	conn, e := grpc.Dial(
		addr,
		opts...,
	)
	if e != nil {
		return nil, e
	}
	return &_Dialer{conn: conn}, nil
}

// Repeat 斷線後 需要 手動 重撥
func (d *_Dialer) Repeat() bool {
	return false
}

// Dial 向服務器 撥號
func (d *_Dialer) Dial() (session transport.Session, e error) {
	if d.conn == nil {
		e = errConnMiss
		return
	}
	session = &_Session{
		c: d.conn,
	}
	d.conn = nil
	return
}
