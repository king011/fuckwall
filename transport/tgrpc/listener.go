package tgrpc

import (
	"context"
	"errors"
	fuckwall_grpc "gitlab.com/king011/fuckwall/protocol/grpc"
	"gitlab.com/king011/fuckwall/transport"
	tgrpc_transport "gitlab.com/king011/fuckwall/transport/tgrpc/transport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	"net"
)

// Transport 傳輸 通道 名稱
const Transport = "grpc"

var errListenerClosed = errors.New("listener closed")

type _Listener struct {
	s *grpc.Server
	l net.Listener

	ctx    context.Context
	cancel context.CancelFunc

	session chan *_Session
	stream  chan *_Stream
}

// NewListener 創建監聽 機器
func NewListener(password []byte, addr string,
	certFile, keyFile string,
) (listener transport.Listener, e error) {
	// 創建 tcp Listener
	var l net.Listener
	l, e = net.Listen("tcp", addr)
	if e != nil {
		l.Close()
		return
	}

	// 創建 grpc 服務器
	var s *grpc.Server
	if certFile != "" && keyFile != "" {
		var creds credentials.TransportCredentials
		creds, e = credentials.NewServerTLSFromFile(certFile, keyFile)
		if e != nil {
			l.Close()
			return
		}
		s = grpc.NewServer(
			grpc.Creds(
				tgrpc_transport.NewServerCreds(
					creds,
					password,
				),
			),
		)

	} else {
		{
			s = grpc.NewServer(
				grpc.Creds(
					tgrpc_transport.NewServerCreds(
						nil,
						password,
					),
				),
			)
		}
	}
	// 註冊 grpc 服務
	ctx, cancel := context.WithCancel(context.Background())
	gl := &_Listener{
		s:       s,
		l:       l,
		ctx:     ctx,
		cancel:  cancel,
		session: make(chan *_Session),
		stream:  make(chan *_Stream),
	}
	fuckwall_grpc.RegisterTransportServer(s, &_Transport{
		l: gl,
	})
	// 註冊路由
	reflection.Register(s)

	// 運行 服務器
	go s.Serve(l)
	go gl.SendSession()
	listener = gl
	return
}
func (l *_Listener) SendSession() {
	// 創建 session
	session := &_Session{
		l: l,
	}
	// 發送 session
	select {
	case <-l.ctx.Done():
	case l.session <- session:
	}
}
func (l *_Listener) Close() error {
	l.s.Stop()
	l.cancel()
	return l.l.Close()
}
func (l *_Listener) Addr() net.Addr {
	return l.l.Addr()
}
func (l *_Listener) Accept() (session transport.Session, e error) {
	select {
	case <-l.ctx.Done():
		e = errListenerClosed
	case session = <-l.session:
	}
	return
}
func (l *_Listener) getStream() (stream *_Stream, e error) {
	select {
	case <-l.ctx.Done():
		e = errListenerClosed
	case stream = <-l.stream:
	}
	return
}
