package tgrpc

import (
	"context"
	"errors"
	fuckwall_grpc "gitlab.com/king011/fuckwall/protocol/grpc"
	"sync"
)

var errStreamClosed = errors.New("stream closed")

type _Stream struct {
	srv    fuckwall_grpc.Transport_RawServer
	client fuckwall_grpc.Transport_RawClient

	ctx    context.Context
	cancel context.CancelFunc
	closed bool

	sync.Mutex
	buffer []byte
}

// Transport 返回 傳輸協議 名稱
func (s *_Stream) Transport() string {
	return Transport
}
func (s *_Stream) Write(b []byte) (n int, e error) {
	if len(b) == 0 {
		return
	}
	if s.srv == nil {
		e = s.client.Send(&fuckwall_grpc.RawRequest{
			Data: b,
		})
	} else {
		e = s.srv.Send(&fuckwall_grpc.RawResponse{
			Data: b,
		})
	}

	if e != nil {
		return
	}
	n = len(b)
	return
}
func (s *_Stream) Read(b []byte) (n int, e error) {
	if len(b) == 0 {
		return
	}
	n, e = s.unsafeRead(b)
	return
}
func (s *_Stream) unsafeRead(b []byte) (n int, e error) {
	if s.buffer == nil {
		if s.srv == nil {
			var response fuckwall_grpc.RawResponse
			for {
				e = s.client.RecvMsg(&response)
				if e != nil {
					return
				}
				if len(response.Data) != 0 {
					s.buffer = response.Data
					break
				}
			}
		} else {
			var request fuckwall_grpc.RawRequest
			for {
				e = s.srv.RecvMsg(&request)
				if e != nil {
					return
				}
				if len(request.Data) != 0 {
					s.buffer = request.Data
					break
				}
			}
		}
	}
	n = copy(b, s.buffer)
	if n != 0 {
		s.buffer = s.buffer[n:]
		if len(s.buffer) == 0 {
			s.buffer = nil
		}
	}
	return
}
func (s *_Stream) Close() (e error) {
	s.Lock()
	if s.closed {
		s.Unlock()
		e = errStreamClosed
		return
	}
	s.closed = true
	s.Unlock()

	s.cancel()
	return
}
