package tgrpc

import (
	"context"
	"fmt"
	fuckwall_grpc "gitlab.com/king011/fuckwall/protocol/grpc"
)

type _Transport struct {
	l *_Listener
}

// Raw 傳輸 bytes 的雙向流
func (t *_Transport) Raw(srv fuckwall_grpc.Transport_RawServer) (e error) {
	ctx1, cancel := context.WithCancel(context.Background())
	stream := &_Stream{
		srv:    srv,
		ctx:    ctx1,
		cancel: cancel,
	}
	go t.pushStream(stream)

	// 等待 完成
	ctx := srv.Context()
	select {
	case <-ctx.Done():
		e = ctx.Err()
		fmt.Println("ctx")
	case <-ctx1.Done():
		e = ctx.Err()
		fmt.Println("ctx1")
	}
	cancel()
	return nil
}
func (t *_Transport) pushStream(stream *_Stream) {
	select {
	case <-stream.ctx.Done():
	case t.l.stream <- stream:
	}
}
