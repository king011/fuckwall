package tgrpc

import (
	"context"
	"errors"
	fuckwall_grpc "gitlab.com/king011/fuckwall/protocol/grpc"
	"gitlab.com/king011/fuckwall/transport"
	"google.golang.org/grpc"
	"sync"
)

var errSessionClosed = errors.New("grpc session closed")
var errNotSupportAcceptStream = errors.New("grpc client session not support AcceptStream")
var errNotSupportOpenStream = errors.New("grpc server session not support OpenStream")

type _Session struct {
	l      *_Listener
	c      *grpc.ClientConn
	closed bool
	sync.Mutex
}

// Transport 返回 傳輸協議 名稱
func (s *_Session) Transport() string {
	return Transport
}

// AcceptStream 接受一個 客戶端 的 Stream 創建
func (s *_Session) AcceptStream() (transport.Stream, error) {
	if s.l == nil {
		return nil, errNotSupportAcceptStream
	}
	return s.l.getStream()
}

// OpenStream 客戶端 向服務器 申請 創建一個 Stream
func (s *_Session) OpenStream() (transport.Stream, error) {
	if s.c == nil {
		return nil, errNotSupportOpenStream
	}

	client := fuckwall_grpc.NewTransportClient(s.c)
	ctx, cancel := context.WithCancel(context.Background())
	stream, e := client.Raw(ctx)
	if e != nil {
		cancel()
		return nil, e
	}

	return &_Stream{
		client: stream,
		ctx:    ctx,
		cancel: cancel,
	}, nil
}
func (s *_Session) Close() (e error) {
	s.Lock()
	if s.closed {
		s.Unlock()
		e = errSessionClosed
		return
	}
	s.closed = true
	s.Unlock()

	if s.l != nil { // 服務器 session
		go s.l.SendSession()
	}
	if s.c != nil { // 客戶端 session
		e = s.c.Close()
	}
	return
}
