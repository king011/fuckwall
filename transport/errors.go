package transport

import (
	"errors"
)

// ErrPermissionDenied 沒有權限 提供是客戶端 在沒意思登入就發送 其它請求
var ErrPermissionDenied = errors.New("permission denied")

// ErrPasswordNotMatch 密碼不匹配 通常是 客戶端發服務器配置了 不同的密碼
var ErrPasswordNotMatch = errors.New("password not match")
