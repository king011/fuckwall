package transport

const (
	// CommnadLogin 客戶端 請求 登入系統
	//
	// body 是 驗證 密碼 sha512 值
	//
	// 成功 服務器 返回 CommnadSuccess
	// 失敗 服務器 返回 CommnadError
	CommnadLogin = 1

	// CommnadPing 客戶端 向服務器發送一個 數據包 以用來測試 速度
	//
	// body 是任意 數據 服務器 原樣返回
	//
	// 成功 服務器 返回 CommnadSuccess
	// 失敗 服務器 返回 CommnadError
	CommnadPing = 2

	// CommandRuntime 客戶端 向服務器請求 返回 服務器運行 信息
	//
	// body 爲 nil
	// 成功 服務器 返回 CommnadSuccess + RuntimeResponse
	// 失敗 服務器 返回 CommnadError
	CommandRuntime = 3

	// CommnadConnect 客戶端 請求 建立目標地址間的 代理通道
	//
	// body 是 目標地址字符串 domain:port
	// 成功 服務器 返回 CommnadSuccess
	// 失敗 服務器 返回 CommnadError
	CommnadConnect = 10
	// CommnadDNS 客戶端 請求 服務器 轉發 dns 請求
	//
	// body 爲 DNSRequest
	//
	// 成功 服務器 返回 CommnadSuccess + dns 響應數據
	// 失敗 服務器 返回 CommnadError
	CommnadDNS = 11

	// CommnadSuccess 成功
	//
	// body 依據請求不同 而不同
	CommnadSuccess = 200
	// CommnadError 失敗
	//
	// body 是可選的 字符串 錯誤描述
	CommnadError = 500
)
