// Package transport 定義了 傳輸層接口
package transport

import (
	"io"
	"net"
)

// Stream 全雙工流
type Stream interface {
	io.Reader
	io.Writer
	io.Closer
	// Transport 返回 傳輸協議 名稱
	Transport() string
}

// Session 網路 連接
type Session interface {
	// AcceptStream 接受一個 客戶端 的 Stream 創建
	AcceptStream() (Stream, error)
	// OpenStream 客戶端 向服務器 申請 創建一個 Stream
	OpenStream() (Stream, error)
	// Transport 返回 傳輸協議 名稱
	Transport() string
	io.Closer
}

// Listener like net.Listener
type Listener interface {
	// Close 關閉服務器
	Close() error
	// Addr 返回 服務器 監聽 地址
	Addr() net.Addr
	// Accept 返回 session
	Accept() (Session, error)
}

// Dialer 撥號器
type Dialer interface {
	// Dial 向服務器 撥號
	Dial() (Session, error)

	// Repeat 如果返回 false 則不需要 重新 撥號 斷線後會自動重撥
	Repeat() bool
}
