#!/bin/bash

dir=$(cd $(dirname $BASH_SOURCE)/../../../ && pwd)
if [[ "$OSTYPE" == "msys" ]]; then
    output=${dir:1:1}:${dir:2}/
else
    output=$dir/
fi
./grpc.sh go pb/ $output