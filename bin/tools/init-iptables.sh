#!/bin/bash

# 設置 dns 監聽 端口
# 請修改爲你的 client.jsonnet 中的 DNS 配置端口
# 如果使用 CoreDNS 則爲 CoreDNS 中的配置 端口
DNS_Port=10053
# 設置 redir 監聽端口 
# 請修改爲你的 client.jsonnet 中的 Redir 配置端口
Redir_Port=10900
# 設置 redir 訪問的 外部地址 以便放行
# 請修改爲你的 client.jsonnet 中的 Remote.Addr 配置的 服務器地址 端口
Redir_Dst=65.49.214.171
Redir_Dst_Port=10700


# 清空 sudo iptables 規則
sudo iptables -t nat -F
sudo iptables -t filter -F
sudo iptables -t mangle -F

# 創建 nat/tcp 轉發鏈 用於 轉發 tcp流
sudo iptables-save | egrep "^\:NAT-TCP" >> /dev/null
if [[ $? != 0 ]];then
	sudo iptables -t nat -N NAT-TCP
fi

# 放行 到 redir 的tcp連接
sudo iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport $Redir_Port -j ACCEPT

# 將 dns 查詢 轉到 $DNS_Port
sudo iptables -t nat -A OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port
sudo iptables -t nat -A OUTPUT -p tcp -m tcp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port

# 放行 環回地址 保留地址 特殊地址
sudo iptables -t nat -A NAT-TCP -d 0/8 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 127/8 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 10/8 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 169.254/16 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 172.16/12 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 192.168/16 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 224/4 -j RETURN
sudo iptables -t nat -A NAT-TCP -d 240/4 -j RETURN

# 放行 發往 服務器的 數據
#sudo iptables -t nat -A NAT-TCP -d $Redir_Dst -p tcp --dport $Redir_Dst_Port -j RETURN
#sudo iptables -t nat -A NAT-TCP -d $Redir_Dst -p udp --dport $Redir_Dst_Port -j RETURN
sudo iptables -t nat -A NAT-TCP -d $Redir_Dst -j RETURN

# 重定向 tcp 數據包到 redir 監聽端口
sudo iptables -t nat -A NAT-TCP -p tcp -j REDIRECT --to-ports $Redir_Port

# 本機 tcp 數據包流向 NAT-TCP 鏈
sudo iptables -t nat -A OUTPUT -p tcp -j NAT-TCP

# 內網 tcp 數據包流向 NAT-TCP 鏈
sudo iptables -t nat -A PREROUTING -p tcp -s 192.168/16 -j NAT-TCP


# 內網數據包源 NAT
sudo iptables -t nat -A POSTROUTING -s 192.168/16 -j MASQUERADE
