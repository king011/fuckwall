#!/bin/bash

iptables -t nat -F
iptables -t filter -F
iptables -t mangle -F

iptables -t nat -A OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:10053
iptables -t nat -A OUTPUT -p tcp -m tcp --dport 53 -j DNAT --to-destination 127.0.0.1:10053
