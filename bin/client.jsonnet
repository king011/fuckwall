{
	// 服務器 設置
	Remote:{
		// 傳輸協議
		//Transport:"quic",
		Transport:"grpc",
		// 服務器 地址
		Addr:"127.0.0.1:10700",
		// 密碼
		Password:"cerberus is an idea",
		// 是否使用 TLS 安全 傳輸
		// 對於 quic TLS 始終爲 true
		TLS:false,
		// 是否 忽略 證書驗證
		SkipVerify:true,
	},
	// dns 配置
	// 如果 爲空 則 不轉發 dns 請求
	DNS:{
		// 本地 dns 服務器地址
		LAddr:"127.0.0.1:1054",
		// dns 服務器地址
		Server:"8.8.8.8:53",
	},
	// socks5 監聽地址
	// 如果 爲空 則不創建 socks5 代理
	Socks5:"127.0.0.1:1080",
	// iptables 透明代理 監聽地址
	// 如果 爲空 則不創建 redir 代理
	Redir:":10900",
	
	// 路網 配置
	NET:{
		// 請求超時 時間 (單位:秒)
		// 默認 10 秒
		Request:10,
		// 網橋 未活躍 超時 斷線時間  (單位:秒)
		// 默認 5 分鐘
		Bridge:60*5,
		// 每個連接 網路數據包 緩衝區大小
		// 默認 1024 * 32
		Buffer:1024*32
	},
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20800",
		// 日誌 檔案名 如果爲空 則輸出到控制檯
		//Filename:"logs/socks5.log",
		// 單個日誌檔案 大小上限 MB
		//MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		//MaxBackups: 3,
		// 保存 多少天內的 日誌
		//MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
		Caller:true,
	},
}