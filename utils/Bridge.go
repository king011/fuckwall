package utils

import (
	"context"
	"io"
	"sync"
	"sync/atomic"
	"time"
)

type _Bridge struct {
	sync.Mutex
	last   time.Time
	cancel context.CancelFunc
}

// Bridge 橋接 c0 c1
func Bridge(c0, c1 io.ReadWriteCloser,
	bufferLen int,
	timer time.Duration, timeout time.Duration,
	num0, num1 *int64,
) {
	ctx, cancel := context.WithCancel(context.Background())
	bridge := &_Bridge{
		last:   time.Now(),
		cancel: cancel,
	}
	go pipeBridge(bridge, c0, c1, bufferLen, num0)
	go pipeBridge(bridge, c1, c0, bufferLen, num1)
	t := time.NewTimer(timer)
	for {
		select {
		case <-ctx.Done():
			if !t.Stop() {
				<-t.C
			}
			return
		case <-t.C:
			bridge.Lock()
			last := bridge.last
			bridge.Unlock()
			if time.Now().After(last.Add(timeout)) {
				// 5 分鐘 沒有 上下行 流量 關閉 stream
				c0.Close()
				c1.Close()
				return
			}
			t.Reset(timer)
		}
	}
}
func pipeBridge(bridge *_Bridge, c0, c1 io.ReadWriteCloser, bufferLen int, num *int64) {
	if num != nil {
		atomic.AddInt64(num, 1)
		defer atomic.AddInt64(num, -1)
	}
	b := make([]byte, bufferLen)
	var n int
	var e error
	for {
		n, e = c0.Read(b)
		if e != nil {
			break
		}
		if n == 0 {
			continue
		}
		bridge.Lock()
		bridge.last = time.Now()
		bridge.Unlock()
		_, e = c1.Write(b[:n])
		if e != nil {
			break
		}
	}
	c0.Close()
	c1.Close()
	bridge.cancel()
}
