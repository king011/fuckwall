package utils

import (
	"errors"
	"fmt"

	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/king-go/io/message"
)

// CheckResponse 驗證 響應信息
func CheckResponse(response []byte) (e error) {
	cmd := message.Command(response)
	switch cmd {
	case transport.CommnadSuccess:
	case transport.CommnadError:
		body := message.Body(response)
		e = errors.New(string(body))
	default:
		e = fmt.Errorf("unknow result code [%v]", cmd)
	}
	return
}
