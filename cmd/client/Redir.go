package client

import (
	"log"
	"net"
	"runtime"
	"syscall"
	"time"

	native_sys "gitlab.com/king011/fuckwall/cmd/client/sys"
	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/utils"
	"go.uber.org/zap"
)

// Redir 透明代理
type Redir struct {
	l             net.Listener
	connectBridge ConnectBridge
	net           *configure.NET
	proxy         *int64
	request       *int64
}

// NewRedir 創建一個 redir 服務器
func NewRedir(laddr string, bridge ConnectBridge, netCnf *configure.NET, proxy, request *int64) (s *Redir, e error) {
	var l net.Listener
	l, e = net.Listen("tcp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewRedir"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if !logger.Logger.OutConsole() {
		log.Println("socks5 work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "socks5 running"); ce != nil {
		ce.Write(
			zap.String("laddr", laddr),
		)
	}

	s = &Redir{
		l:             l,
		connectBridge: bridge,
		net:           netCnf,
		proxy:         proxy,
		request:       request,
	}
	return
}

// Run 運行 服務
func (r *Redir) Run() {
	l := r.l
	for {
		c, e := l.Accept()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Accept"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.Int("NumGoroutine", runtime.NumGoroutine()),
				)
			}
			time.Sleep(time.Second)
			continue
		}
		go r.initAccept(c)
	}
}
func (r *Redir) initAccept(c net.Conn) {
	defer c.Close()
	// 返回 fd
	sys, ok := c.(syscall.Conn)
	if !ok {
		logger.Logger.Warn("Redir net.Conn not implemented syscall.Conn")
		return
	}
	var sysc syscall.RawConn
	sysc, e := sys.SyscallConn()
	if !ok {
		if ce := logger.Logger.Check(zap.WarnLevel, "SyscallConn"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 返回 原目標
	var addr *net.TCPAddr
	ec := sysc.Control(func(fd uintptr) {
		addr = native_sys.GetDestAddr(int(fd))
	})
	if ec != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Control fd"); ce != nil {
			ce.Write(
				zap.Error(ec),
			)
		}
		return
	}
	if addr == nil {
		logger.Logger.Warn("native getsockopt error")
		return
	}
	// 連接 目標
	stream, e := r.connectBridge.ConnectBridge(addr.String())
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Redir Connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 建立 橋接
	utils.Bridge(c, stream,
		r.net.Buffer,
		time.Second*5, r.net.Bridge,
		r.proxy, r.request,
	)
}
