package client

import (
	"crypto/sha512"
	"os"

	"gitlab.com/king011/fuckwall/configure"

	"gitlab.com/king011/fuckwall/logger"
	"go.uber.org/zap"
	"runtime"
)

// Run 運行 服務器
func Run(cnf *configure.Client, debug bool) {
	// 創建 服務
	s := Service{
		debug: debug,
		net:   &cnf.NET,
	}

	// 初始化 撥號器
	pwd := sha512.Sum512([]byte(cnf.Remote.Password))
	switch cnf.Remote.Transport {
	case "quic":
		e := s.InitQuic(&cnf.Remote, pwd[:])
		if e != nil {
			os.Exit(1)
		}
	case "grpc":
		e := s.InitGRPC(&cnf.Remote, pwd[:])
		if e != nil {
			os.Exit(1)
		}
	default:
		if ce := logger.Logger.Check(zap.FatalLevel, "not support transport"); ce != nil {
			ce.Write(
				zap.String("val", cnf.Remote.Transport),
			)
		}
		os.Exit(1)
	}

	// 初始化 socks5 服務器
	if cnf.Socks5 != "" {
		var socks5 *Socks5
		var e error
		if debug {
			socks5, e = NewSocks5(cnf.Socks5, &s, &cnf.NET, &s.trace.SocksProxy, &s.trace.SocksRequest)
		} else {
			socks5, e = NewSocks5(cnf.Socks5, &s, &cnf.NET, nil, nil)
		}
		if e != nil {
			os.Exit(1)
			return
		}
		s.Socks5 = socks5
	}
	// 初始化 redir 服務器
	if cnf.Redir != "" && runtime.GOOS != "windows" {
		var redir *Redir
		var e error
		if debug {
			redir, e = NewRedir(cnf.Redir, &s, &cnf.NET, &s.trace.RedirProxy, &s.trace.RedirRequest)
		} else {
			redir, e = NewRedir(cnf.Redir, &s, &cnf.NET, nil, nil)
		}
		if e != nil {
			os.Exit(1)
			return
		}
		s.Redir = redir
	}

	// 初始化 dns 服務器
	if cnf.DNS != nil {
		var dns *DNS
		var e error
		if debug {
			dns, e = NewDNS(cnf.DNS.LAddr, cnf.DNS.Server, &s, &s.trace.DNSTCP, &s.trace.DNSUDP)
		} else {
			dns, e = NewDNS(cnf.DNS.LAddr, cnf.DNS.Server, &s, nil, nil)
		}
		if e != nil {
			os.Exit(1)
			return
		}
		s.DNS = dns
	}

	// 運行 服務
	s.Run()
}
