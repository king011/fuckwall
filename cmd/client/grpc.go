package client

import (
	"crypto/tls"

	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport/tgrpc"
	"go.uber.org/zap"
)

// InitGRPC 初始化 grpc 撥號器
func (s *Service) InitGRPC(cnf *configure.Remote, pwd []byte) (e error) {
	// 創建 撥號器
	dialer, e := tgrpc.NewDialer(pwd,
		cnf.Addr,
		&tls.Config{
			InsecureSkipVerify: cnf.SkipVerify,
		},
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDialer"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 撥號
	session, e := dialer.Dial()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "Dial"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	s.dialer = dialer
	s.session = session
	return
}
