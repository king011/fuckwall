package client

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/king011/fuckwall/logger"
	"go.uber.org/zap"
	"io"
	"net"
)

var errNotBufferReadUID = errors.New("not buffer read uid")

func (s *Socks5) initAccept4a(c io.ReadWriter, b []byte) (addr string, response []byte, e error) {
	// 讀取 目標
	e = readBuffer(c, b[2:8])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks4a Request"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	b[0] = 0
	switch b[1] {
	case 0x01:
		addr, response, e = s.readSocks4(c, b)
		return
	case 0x02:
		e = ErrNotSupportBIND
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks4a Request"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	default:
		e = fmt.Errorf("not support CMD [%v]", b[1])
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks4a Request unknow CMD"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	b[1] = 91
	// 通知 失敗
	response = b[:8]
	return
}
func (s *Socks5) readSocks4(c io.ReadWriter, b []byte) (addr string, response []byte, e error) {
	port := binary.BigEndian.Uint16(b[2:])
	target := net.IPv4(b[4], b[5], b[6], b[7]).String()
	if b[4] == 0 && b[5] == 0 && b[6] == 0 && b[7] != 0 {
		addr, response, e = s.initSocks4a(c, b, port)
	} else {
		response, e = s.initSocks4(c, b)
		if e == nil {
			addr = fmt.Sprintf("%v:%v", target, port)
		}
	}
	return
}
func (s *Socks5) initSocks4a(c io.ReadWriter, b []byte, port uint16) (addr string, response []byte, e error) {
	// 讀取 uid
	buf := b[8:]
	pos := 0
	var n int
	first := -1
	for {
		if pos == len(buf) {
			e = errNotBufferReadUID
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4a"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		}
		n, e = c.Read(buf[pos:])
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4a"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		} else if n == 0 {
			continue
		}
		pos += n
		flag := pos - 1
		if buf[flag] == 0 {
			if first != -1 {
				break
			}
			first = bytes.IndexByte(buf[:pos], 0)
			if first != flag {
				break
			}
		}
	}
	buf = buf[first:pos]

	addr = fmt.Sprintf("%s:%v", buf, port)
	response = b[:8]
	response[1] = 90
	return
}
func (s *Socks5) initSocks4(c io.ReadWriter, b []byte) (response []byte, e error) {
	// 讀取 uid
	buf := b[8:]
	pos := 0
	var n int
	for {
		if pos == len(buf) {
			e = errNotBufferReadUID
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		}
		n, e = c.Read(buf[pos:])
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		} else if n == 0 {
			continue
		}
		pos += n
		if buf[pos-1] == 0 {
			break
		}
	}
	response = b[:8]
	response[1] = 90
	return
}
