package client

import (
	"context"
	"fmt"
	"io"
	"math"
	"runtime"
	"strconv"
	"time"

	"github.com/golang/protobuf/proto"
	command "gitlab.com/king011/fuckwall/protocol/command"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	"gitlab.com/king011/fuckwall/version"
	king_io "gitlab.com/king011/king-go/io"
	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
)

type _Trace struct {
	Stream       int64
	DNSTCP       int64
	DNSUDP       int64
	SocksProxy   int64
	SocksRequest int64
	RedirProxy   int64
	RedirRequest int64
}

func (s *Service) displayHelp() {
	fmt.Fprint(s.reader.Out,
		`h/help     display help
l/local    display runtime info
r/remote   display remote runtime info
p/ping     send number length data for ping
`,
	)
}
func (s *Service) runDebug() {
	s.reader = king_io.NewInputReader()
	var cmd string
	s.displayHelp()
	for {
		cmd = s.reader.ReadString("cmd")
		if cmd == "h" || cmd == "help" {
			s.displayHelp()
		} else if cmd == "l" || cmd == "local" {
			s.displayLocal()
		} else if cmd == "r" || cmd == "remote" {
			s.displayRemote()
		} else if cmd == "p" || cmd == "ping" {
			s.debugPing()
		} else {
			fmt.Fprint(s.reader.Err,
				`not support command
Use "help" for more information about this program
`)
		}
	}
}
func (s *Service) displayLocal() {
	if s.debug {
		fmt.Fprintf(s.reader.Out,
			`******   Local   ******

Runtime      : %v %v %v
Version      : %v %v
Build        : %v

Running      : %v
CPU          : %v
Goroutine    : %v
CgoCall      : %v

DNSTCP       : %v
DNSUDP       : %v
SocksProxy   : %v
SocksRequest : %v
RedirProxy   : %v
RedirRequest : %v
`,
			runtime.GOOS, runtime.GOARCH, runtime.Version(),
			version.Tag, version.Commit,
			version.Date,
			time.Now().Sub(s.started),
			runtime.NumCPU(),
			runtime.NumGoroutine(),
			runtime.NumCgoCall(),

			s.trace.DNSTCP,
			s.trace.DNSUDP,
			s.trace.SocksProxy,
			s.trace.SocksRequest,
			s.trace.RedirProxy,
			s.trace.RedirRequest,
		)
	} else {
		fmt.Fprintf(s.reader.Out,
			`******   Local   ******

Runtime   : %v %v %v
Version   : %v %v
Build     : %v

Running   : %v
CPU       : %v
Goroutine : %v
CgoCall   : %v

`,
			runtime.GOOS, runtime.GOARCH, runtime.Version(),
			version.Tag, version.Commit,
			version.Date,
			time.Now().Sub(s.started),
			runtime.NumCPU(),
			runtime.NumGoroutine(),
			runtime.NumCgoCall(),
		)
	}
}
func (s *Service) displayRemote() {
	session, e := s.getSession()
	if e != nil {
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	stream, e := session.OpenStream()
	if e != nil {
		s.daelError(session, e)
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	msg, e := message.Marshal(transport.CommandRuntime, nil)
	if e != nil {
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer func() {
		stream.Close()
		cancel()
	}()
	client := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	msg, e = client.Call(ctx, msg)
	if e != nil {
		if e != io.EOF {
			s.daelError(session, e)
		}
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	e = utils.CheckResponse(msg)
	if e != nil {
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	var response command.RuntimeResponse
	e = proto.Unmarshal(message.Body(msg), &response)
	if e != nil {
		fmt.Fprintln(s.reader.Err, e)
		return
	}
	started := time.Unix(response.Started, 0)
	if response.Debug {
		fmt.Fprintf(s.reader.Out,
			`******   Remote   ******

Runtime   : %v %v %v
Version   : %v %v
Build     : %v

Running   : %v
CPU       : %v
Goroutine : %v
CgoCall   : %v

Session   : %v
Stream    : %v
Proxy     : %v
Request   : %v

`,
			response.GOOS, response.GOARCH, response.Version,
			response.Tag, response.Commit,
			response.Date,

			time.Now().Sub(started),
			response.NumCPU,
			response.NumGoroutine,
			response.NumCgoCall,

			response.Session,
			response.Stream,
			response.Proxy,
			response.Request,
		)
	} else {
		fmt.Fprintf(s.reader.Out,
			`******   Remote   ******

Runtime   : %v %v %v
Version   : %v %v
Build     : %v

Running   : %v
CPU       : %v
Goroutine : %v
CgoCall   : %v

`,
			response.GOOS, response.GOARCH, response.Version,
			response.Tag, response.Commit,
			response.Date,

			time.Now().Sub(started),
			response.NumCPU,
			response.NumGoroutine,
			response.NumCgoCall,
		)
	}
	return
}

func (s *Service) debugPing() {
	fmt.Fprintln(s.reader.Out, `Input ping data len
Enter b to return`)
	for {
		str := s.reader.ReadString("ping")
		if str == "b" {
			break
		}
		n, _ := strconv.ParseUint(str, 10, 16)
		last := time.Now()
		e := s.doneDebugPing(int(n))
		if e != nil {
			fmt.Fprintln(s.reader.Err, e)
		}
		fmt.Fprintln(s.reader.Out, n, time.Now().Sub(last))
	}
	return
}
func (s *Service) doneDebugPing(n int) (e error) {
	session, e := s.getSession()
	if e != nil {
		return
	}
	stream, e := session.OpenStream()
	if e != nil {
		s.daelError(session, e)
		return
	}
	msg := make([]byte, message.Len(n))
	if !message.Format(msg, transport.CommnadPing) {
		e = fmt.Errorf("body must len == %v,not support %v", math.MaxUint16, n)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer func() {
		stream.Close()
		cancel()
	}()
	client := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	msg, e = client.Call(ctx, msg)
	if e != nil {
		if e != io.EOF {
			s.daelError(session, e)
		}
		return
	}
	e = utils.CheckResponse(msg)
	return
}
