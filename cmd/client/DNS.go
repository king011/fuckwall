package client

import (
	"log"
	"net"
	"sync"
	"sync/atomic"

	"github.com/golang/protobuf/proto"
	"github.com/miekg/dns"
	"gitlab.com/king011/fuckwall/logger"
	command "gitlab.com/king011/fuckwall/protocol/command"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	"gitlab.com/king011/king-go/io/message"
	"go.uber.org/zap"
)

// ForwardDNS 轉發 dns 請求
type ForwardDNS interface {
	ForwardDNS([]byte) ([]byte, error)
}

// DNS 服務器
type DNS struct {
	l       net.Listener
	p       net.PacketConn
	server  string
	forward ForwardDNS

	tcp, udp *int64
}

// NewDNS 創建 dns 服務器
func NewDNS(laddr, server string, forward ForwardDNS, tcp, udp *int64) (d *DNS, e error) {
	// 監聽 udp
	u, e := net.ResolveUDPAddr("udp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	p, e := net.ListenUDP("udp", u)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if !logger.Logger.OutConsole() {
		log.Println("dns udp work at", laddr, "->", server)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "dns udp running"); ce != nil {
		ce.Write(
			zap.String("laddr", laddr),
			zap.String("dns", server),
		)
	}

	// 監聽 tcp
	l, e := net.Listen("tcp", laddr)
	if e != nil {
		p.Close()
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if !logger.Logger.OutConsole() {
		log.Println("dns tcp work at", laddr, "->", server)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "dns tcp running"); ce != nil {
		ce.Write(
			zap.String("laddr", laddr),
			zap.String("dns", server),
		)
	}

	// 返回 服務器
	d = &DNS{
		p:       p,
		l:       l,
		server:  server,
		forward: forward,
		tcp:     tcp,
		udp:     udp,
	}
	return
}

// Run 運行 dns 服務
func (d *DNS) Run() {
	if d.l == nil || d.p == nil {
		return
	}

	var wait sync.WaitGroup
	wait.Add(2)
	go func() {
		dns.ActivateAndServe(d.l, nil,
			&DNSHandler{
				TCP:     true,
				server:  d.server,
				forward: d.forward,
				num:     d.tcp,
			},
		)
		wait.Done()
	}()
	go func() {
		dns.ActivateAndServe(nil, d.p,
			&DNSHandler{
				TCP:     false,
				server:  d.server,
				forward: d.forward,
				num:     d.udp,
			},
		)
		wait.Done()
	}()
	wait.Wait()
}

// DNSHandler .
type DNSHandler struct {
	TCP     bool
	server  string
	forward ForwardDNS
	num     *int64
}

// ServeDNS 實現 dns.Handler 接口 響應dns 請求
func (d *DNSHandler) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	if d.num != nil {
		atomic.AddInt64(d.num, 1)
		defer atomic.AddInt64(d.num, -1)
	}
	// 創建 請求
	b, e := r.Pack()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "ServeDNS Pack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	b, e = proto.Marshal(&command.DNSRequest{
		DNS:  d.server,
		Data: b,
		TCP:  d.TCP,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "ServeDNS proto.Marshal"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	msg, e := message.Marshal(transport.CommnadDNS, b)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "ServeDNS message.Marshal"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 發送 請求
	msg, e = d.forward.ForwardDNS(msg)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "ServeDNS ForwardDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	e = utils.CheckResponse(msg)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "ServeDNS ForwardDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 返回 響應
	var in dns.Msg
	e = in.Unpack(message.Body(msg))
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "ServeDNS Unpack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	e = w.WriteMsg(&in)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "ServeDNS Write"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
}
