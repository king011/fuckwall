package client

import (
	"crypto/tls"

	quic "github.com/lucas-clemente/quic-go"
	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport/tquic"
	"go.uber.org/zap"
)

// InitQuic 初始化 quic 撥號器
func (s *Service) InitQuic(cnf *configure.Remote, pwd []byte) (e error) {
	// 創建 撥號器
	dialer := tquic.NewDialer(pwd,
		cnf.Addr,
		&tls.Config{
			InsecureSkipVerify: cnf.SkipVerify,
		},
		&quic.Config{
			KeepAlive: true,
		},
	)
	// 撥號
	session, e := dialer.Dial()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "Dial"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	s.dialer = dialer
	s.session = session
	return
}
