package client

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"runtime"
	"time"

	"encoding/binary"
	"errors"

	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	kio "gitlab.com/king011/king-go/io"
	"go.uber.org/zap"
)

// ErrNotSupportBIND 不支持 socks5 BIND
var ErrNotSupportBIND = errors.New("not support BIND")

// ErrNotSupportUDP 不支持 socks5 UDP
var ErrNotSupportUDP = errors.New("not support UDP")

// ConnectBridge 創建 網橋
type ConnectBridge interface {
	ConnectBridge(addr string) (stream transport.Stream, e error)
}

// Socks5 代理服務器實現
type Socks5 struct {
	l             net.Listener
	connectBridge ConnectBridge
	net           *configure.NET
	proxy         *int64
	request       *int64
}

// NewSocks5 創建一個 socks5 服務器
func NewSocks5(laddr string, bridge ConnectBridge, netCnf *configure.NET, proxy, request *int64) (s *Socks5, e error) {
	var l net.Listener
	l, e = net.Listen("tcp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewSocks5"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if !logger.Logger.OutConsole() {
		log.Println("socks5 work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "socks5 running"); ce != nil {
		ce.Write(
			zap.String("laddr", laddr),
		)
	}

	s = &Socks5{
		l:             l,
		connectBridge: bridge,
		net:           netCnf,
		proxy:         proxy,
		request:       request,
	}
	return
}

// Run 運行 服務
func (s *Socks5) Run() {
	l := s.l
	for {
		c, e := l.Accept()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Accept"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.Int("NumGoroutine", runtime.NumGoroutine()),
				)
			}
			time.Sleep(time.Second)
			continue
		}
		go s.initAccept(c)
	}
}

func (s *Socks5) initAccept(c net.Conn) {
	defer c.Close()
	ctx, cancel := context.WithTimeout(context.Background(), s.net.Request)
	target, response, e := s.initAcceptContext(ctx, c)
	defer cancel()
	if e != nil {
		if response != nil {
			// 通知 客戶端 失敗
			kio.WriteContext(ctx, c, response)
		}
		return
	}

	// 連接 目標
	stream, e := s.connectBridge.ConnectBridge(target)
	if e != nil {
		return
	}

	// 通知 連接 成功
	_, e = kio.WriteContext(ctx, c, response)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "response connect to client"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 建立 橋接
	utils.Bridge(c, stream,
		s.net.Buffer,
		time.Second*5, s.net.Bridge,
		s.proxy, s.request,
	)
}

type _ResultSocks5 struct {
	Target   string
	Response []byte
	Err      error
}

func (s *Socks5) initAcceptContext(ctx context.Context, c io.ReadWriter) (addr string, response []byte, e error) {
	e = ctx.Err()
	if e != nil {
		return
	}

	done := ctx.Done()
	if done == nil {
		addr, response, e = s.initSocks5(c)
	} else {
		ch := make(chan *_ResultSocks5)
		go func() {
			addr, response, e := s.initSocks5(c)
			ch <- &_ResultSocks5{
				Err:      e,
				Target:   addr,
				Response: response,
			}
		}()

		select {
		case <-done:
			go func() {
				<-ch
			}()
		case rs := <-ch:
			addr, response, e = rs.Target, rs.Response, rs.Err
		}
	}
	return
}
func (s *Socks5) initSocks5(c io.ReadWriter) (addr string, response []byte, e error) {
	// 只要注意下 實現 緩衝區 最大時 就是 接收 socks5請求 時 的 請求/響應 包
	// VER	CMD	RSV	ATYP	DST.ADDR	DST.PORT
	// 1    1   1   1       1+255       2
	//b := make([]byte, 262)
	b := make([]byte, 512)
	e = readBuffer(c, b[:2])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "initSocks5"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if b[0] == 0x4 {
		addr, response, e = s.initAccept4a(c, b)
		return
	} else if b[0] != 0x5 {
		if ce := logger.Logger.Check(zap.WarnLevel, "VER must be 0x05 or 0x04"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[0]),
			)
		}
		return
	}
	if b[1] < 1 || b[1] > 255 {
		if ce := logger.Logger.Check(zap.WarnLevel, "NMETHODS must range [1,255]"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[1]), zap.Uint8("val", b[0]),
			)
		}
		return
	}
	n := b[1]
	e = readBuffer(c, b[:n])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Read METHODS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 查找 支持的 認證
	for _, v := range b[:n] {
		switch v {
		case 0x00: // 無需認證
			// 通知 無需驗證
			b[0] = 0x05
			b[1] = 0x00
			_, e = c.Write(b[:2])
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "Write METHODS"); ce != nil {
					ce.Write(
						zap.Error(e),
					)
				}
				return
			}
			// 等待 客戶端 發送 代理請求
			addr, response, e = s.waitRequest(c, b)
			return
		}
	}

	// 協商 認證 失敗
	b[0] = 0x05
	b[1] = 0xFF
	response = b[:2]
	return
}
func (s *Socks5) waitRequest(c io.Reader, b []byte) (addr string, response []byte, e error) {
	// 等待 客戶端 發送 請求
	e = readBuffer(c, b[:4])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Read Request"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if b[0] != 0x5 {
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Read Request VER must be 0x05"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[0]),
			)
		}
		return
	}
	switch b[1] {
	case 0x01:
		// 返回 CONNECT 目標
		addr, response, e = s.getDst(c, b)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 CONNECT getDst"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
		// 通知 成功
		response[1] = 0x00
		return
	case 0x02:
		e = ErrNotSupportBIND
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Request"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	case 0x03:
		e = ErrNotSupportUDP
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Request"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	default:
		e = fmt.Errorf("not support CMD [%v]", b[1])
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks5 Request unknow CMD"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}

	// 通知 不支持 指令
	b[0] = 0x05
	b[1] = 0x07 // 不支持的 命令
	b[2] = 0
	b[3] = 0x01
	response = b[:4+4+2]
	return
}
func (s *Socks5) getDst(c io.Reader, b []byte) (target string, response []byte, e error) {
	switch b[3] {
	case 0x01: //ipv4
		e = readBuffer(c, b[4:4+4+2])
		if e != nil {
			return
		}
		addr := net.IPv4(b[4], b[5], b[6], b[7]).String()
		port := binary.BigEndian.Uint16(b[4+4:])
		response = b[:4+4+2]
		target = fmt.Sprintf("%v:%v", addr, port)
	case 0x03: //domain
		e = readBuffer(c, b[4:4+1])
		if e != nil {
			return
		}
		n := int(b[4])
		e = readBuffer(c, b[4+1:4+1+n+2])
		addr := string(b[4+1 : 4+1+n])
		port := binary.BigEndian.Uint16(b[4+1+n:])
		//response = b[:4+1+n+2]
		response = b[:4+4+2]
		response[3] = 0x01
		target = fmt.Sprintf("%v:%v", addr, port)
	case 0x04: //ipv6
		e = readBuffer(c, b[4:4+16+2])
		if e != nil {
			return
		}
		addr := net.IP(b[4 : 4+16]).String()
		port := binary.BigEndian.Uint16(b[4+16:])
		//response = b[:4+16+2]
		response = b[:4+4+2]
		response[3] = 0x01
		target = fmt.Sprintf("%v:%v", addr, port)
	default:
		e = fmt.Errorf("not support ATYP [0x%02x]", b[3])
	}
	return
}
func readBuffer(r io.Reader, b []byte) (e error) {
	size := len(b)
	if size == 0 {
		return
	}
	pos := 0
	var n int
	for pos != size {
		n, e = r.Read(b[pos:])
		if e != nil {
			return
		}
		if n == 0 {
			continue
		}
		pos += n
	}
	return
}
