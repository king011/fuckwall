package client

import (
	"context"
	"io"
	"sync"
	"time"

	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	king_io "gitlab.com/king011/king-go/io"
	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
	"go.uber.org/zap"
)

// Service 服務
type Service struct {
	debug bool
	trace _Trace

	started time.Time

	DNS    *DNS
	Socks5 *Socks5
	Redir  *Redir

	dialer  transport.Dialer
	session transport.Session

	reader *king_io.InputReader

	net *configure.NET
	sync.Mutex
}

// Run 運行 服務
func (s *Service) Run() {
	s.started = time.Now()

	var wait sync.WaitGroup
	// 運行 調試 服務
	if s.debug {
		wait.Add(1)
		go func() {
			s.runDebug()
			wait.Done()
		}()
	}
	// 運行 socks5 服務
	if s.Socks5 != nil {
		wait.Add(1)
		go func() {
			s.Socks5.Run()
			wait.Done()
		}()
	}
	// 運行 redir 服務
	if s.Redir != nil {
		wait.Add(1)
		go func() {
			s.Redir.Run()
			wait.Done()
		}()
	}
	// 運行 dns 服務
	if s.DNS != nil {
		wait.Add(1)
		go func() {
			s.DNS.Run()
			wait.Done()
		}()
	}
	wait.Wait()
}

func (s *Service) getSession() (transport.Session, error) {
	s.Lock()
	defer s.Unlock()

	session := s.session
	if session == nil {
		// 重新撥號
		session, e := s.dialer.Dial()
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, "Dial"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return nil, e
		}
		logger.Logger.Info("Dial Success")
		s.session = session
		return session, nil
	}
	return session, nil
}

// ForwardDNS 實現 ForwardDNS 接口
func (s *Service) ForwardDNS(request []byte) (response []byte, e error) {
	session, e := s.getSession()
	if e != nil {
		return
	}

	stream, e := session.OpenStream()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "OpenStream"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.daelError(session, e)
		return
	}
	defer stream.Close()

	client := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	ctx, cancel := context.WithTimeout(context.Background(), s.net.Request)
	response, e = client.Call(ctx, request)
	if e != nil && e != io.EOF {
		s.daelError(session, e)
	}
	cancel()
	return
}
func (s *Service) daelError(session transport.Session, e error) {
	if e == nil || e == context.Canceled || e == context.DeadlineExceeded {
		return
	}

	if ce := logger.Logger.Check(zap.WarnLevel, "Session Error"); ce != nil {
		ce.Write(
			zap.Error(e),
		)
	}

	if !s.dialer.Repeat() {
		// 不需要 手動 撥號
		return
	}

	// 清空 session
	s.Lock()
	if s.session == session {
		s.session = nil
	}
	s.Unlock()
	return
}

// ConnectBridge 實現 ConnectBridge 接口
func (s *Service) ConnectBridge(addr string) (stream transport.Stream, e error) {
	session, e := s.getSession()
	if e != nil {
		return
	}

	stream, e = session.OpenStream()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "OpenStream"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.daelError(session, e)
		return
	}
	defer func() {
		if e != nil {
			stream.Close()
			stream = nil
		}
	}()

	// 請求 建立 連接
	msg, e := message.MarshalString(transport.CommnadConnect, addr)
	if e != nil {
		return
	}
	client := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	ctx, cancel := context.WithTimeout(context.Background(), s.net.Request)
	msg, e = client.Call(ctx, msg)
	cancel()

	// 判斷 響應
	if e == nil {
		e = utils.CheckResponse(msg)
	} else {
		if e != io.EOF {
			s.daelError(session, e)
		}
	}
	return
}
