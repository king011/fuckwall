package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/fuckwall/cmd/client"
	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/utils"
)

func init() {
	basePath := utils.BasePath()
	filename := basePath + "/client.jsonnet"
	var debug bool
	var transport string
	cmd := &cobra.Command{
		Use:   "client",
		Short: "run client",
		Run: func(cmd *cobra.Command, args []string) {
			// 加載 配置
			var cnf configure.Client
			e := cnf.Load(filename)
			if e != nil {
				log.Fatalln(e)
			}
			if transport != "" {
				cnf.Remote.Transport = transport
			}

			// 初始化 日誌
			e = logger.Init(basePath, &cnf.Logger)
			if e != nil {
				log.Fatalln(e)
			}
			client.Run(&cnf, debug)

		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		filename, "configure file path",
	)
	flags.BoolVarP(&debug,
		"debug", "d",
		false, "run as debug mode",
	)
	flags.StringVarP(&transport, "transport", "t", "", "transport grpc/quic")
	rootCmd.AddCommand(cmd)
}
