package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/fuckwall/cmd/server"
	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/utils"
)

func init() {
	basePath := utils.BasePath()
	filename := basePath + "/server.jsonnet"
	var debug bool
	cmd := &cobra.Command{
		Use:   "server",
		Short: "run server",
		Run: func(cmd *cobra.Command, args []string) {
			// 加載 配置
			var cnf configure.Server
			e := cnf.Load(basePath, filename)
			if e != nil {
				log.Fatalln(e)
			}
			// 初始化 日誌
			e = logger.Init(basePath, &cnf.Logger)
			if e != nil {
				log.Fatalln(e)
			}
			server.Run(&cnf, debug)

		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		filename, "configure file path",
	)
	flags.BoolVarP(&debug,
		"debug", "d",
		false, "run as debug mode",
	)
	rootCmd.AddCommand(cmd)
}
