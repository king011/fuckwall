package server

import (
	"log"
	"os"

	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport/tgrpc"
	"go.uber.org/zap"
)

// InitGRPC 初始化 grpc 服務器
func (s *Service) InitGRPC(cnf *configure.Server, pwd []byte) (e error) {
	// 創建 監聽器
	l, e := tgrpc.NewListener(pwd,
		cnf.GRPC.Addr,
		cnf.GRPC.CertFile, cnf.GRPC.KeyFile,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "grpc init"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
	h2 := cnf.GRPC.H2()
	if !logger.Logger.OutConsole() {
		if h2 {
			log.Println("grpc h2 work at", cnf.GRPC.Addr)
		} else {
			log.Println("grpc h2c work at", cnf.GRPC.Addr)
		}

	}
	if h2 {
		if ce := logger.Logger.Check(zap.InfoLevel, "grpc h2 work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.GRPC.Addr),
			)
		}
	} else {
		if ce := logger.Logger.Check(zap.InfoLevel, "grpc h2c work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.GRPC.Addr),
			)
		}
	}
	s.listeners = append(s.listeners, l)
	return
}
