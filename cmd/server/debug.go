package server

import (
	"fmt"
	"runtime"
	"time"

	"gitlab.com/king011/fuckwall/version"
	king_io "gitlab.com/king011/king-go/io"
)

type _Trace struct {
	Session int64
	Stream  int64
	Proxy   int64
	Request int64
}

func (s *Service) displayHelp() {
	fmt.Fprint(s.reader.Out,
		`h/help     display help
l/local    display runtime info
`,
	)
}
func (s *Service) runDebug() {
	s.reader = king_io.NewInputReader()
	var cmd string
	s.displayHelp()
	for {
		cmd = s.reader.ReadString("cmd >")
		if cmd == "l" || cmd == "local" {
			s.displayLocal()
		} else if cmd == "h" || cmd == "help" {
			s.displayHelp()
		} else {
			fmt.Fprint(s.reader.Err,
				`not support command
Use "help" for more information about this program
`)
		}
	}
}
func (s *Service) displayLocal() {
	fmt.Fprintf(s.reader.Out,
		`******   Local   ******

Runtime   : %v %v %v
Version   : %v %v
Build     : %v

Running   : %v
CPU       : %v
Goroutine : %v
CgoCall   : %v

Session   : %v
Stream    : %v
Proxy     : %v
Request   : %v

`,
		runtime.GOOS, runtime.GOARCH, runtime.Version(),
		version.Tag, version.Commit,
		version.Date,

		time.Now().Sub(s.started),
		runtime.NumCPU(),
		runtime.NumGoroutine(),
		runtime.NumCgoCall(),

		s.trace.Session,
		s.trace.Stream,
		s.trace.Proxy,
		s.trace.Request,
	)
}
