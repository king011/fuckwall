package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/miekg/dns"
	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	command "gitlab.com/king011/fuckwall/protocol/command"
	"gitlab.com/king011/fuckwall/transport"
	"gitlab.com/king011/fuckwall/utils"
	"gitlab.com/king011/fuckwall/version"
	king_io "gitlab.com/king011/king-go/io"
	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var errForwardDNSNotEnable = errors.New("forward dns not enable")

// Service 服務
type Service struct {
	debug bool
	trace _Trace
	// 服務 啓動 時間
	started time.Time
	// 是否 啓動 dns 轉發
	dns bool

	listeners []transport.Listener
	reader    *king_io.InputReader

	net *configure.NET
}

// Run 運行 服務
func (s *Service) Run() {
	s.started = time.Now()

	var wait sync.WaitGroup
	// 運行 調試 服務
	if s.debug {
		wait.Add(1)
		go func() {
			s.runDebug()
			wait.Done()
		}()
	}
	if len(s.listeners) != 0 {
		wait.Add(len(s.listeners))
		for _, l := range s.listeners {
			go func(l transport.Listener) {
				s.onServe(l)
				wait.Done()
			}(l)
		}
	}
	wait.Wait()
}
func (s *Service) onServe(l transport.Listener) {
	Tag := "Service.onServe"
	for {
		session, e := l.Accept()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, Tag); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			continue
		}
		go s.onSession(session)
	}
}
func (s *Service) onSession(session transport.Session) {
	Tag := "Service.onSession"

	if s.debug {
		atomic.AddInt64(&s.trace.Session, 1)
	}
	if ce := logger.Logger.Check(zap.DebugLevel, Tag); ce != nil {
		ce.Write(
			zap.String("New", session.Transport()),
		)
	}

	for {
		stream, e := session.AcceptStream()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, Tag); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			break
		}
		go s.onStream(stream)
	}

	if s.debug {
		atomic.AddInt64(&s.trace.Session, -1)
	}
	if ce := logger.Logger.Check(zap.DebugLevel, Tag); ce != nil {
		ce.Write(
			zap.String("Destory", session.Transport()),
		)
	}
	session.Close()
}
func (s *Service) onStream(stream transport.Stream) {
	Tag := "Service.onStream"
	if s.debug {
		atomic.AddInt64(&s.trace.Stream, 1)
	}
	if ce := logger.Logger.Check(zap.DebugLevel, Tag); ce != nil {
		ce.Write(
			zap.String("New", stream.Transport()),
		)
	}
	session := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), stream)
	for {
		e := s.callResponse(session)
		if e != nil {
			break
		}
	}
	if s.debug {
		atomic.AddInt64(&s.trace.Stream, -1)
	}
	if ce := logger.Logger.Check(zap.DebugLevel, Tag); ce != nil {
		ce.Write(
			zap.String("Destory", stream.Transport()),
		)
	}
	stream.Close()
}
func (s *Service) callResponse(session *call.Session) (e error) {
	ctx, cancel := context.WithTimeout(context.Background(), s.net.Request)
	defer cancel()
	request, e := session.Recv(ctx)
	if e != nil {
		if e != io.EOF && e != context.Canceled && grpc.Code(e) != codes.Canceled {
			if ce := logger.Logger.Check(zap.WarnLevel, "Service.callResponse"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
		return
	}
	cmd := message.Command(request)
	switch cmd {
	case transport.CommnadLogin:
		// 在 transport 打開時 就進行了權限 驗證 通常是 網路數據包 丟失 客戶端 沒有得到 驗證通過消息 所以出現 重複驗證
		//
		// 直接 通知 成功
		e = s.responseLogin(ctx, session)
	case transport.CommnadConnect:
		e = s.responseConnect(ctx, session, request)
		if e == nil {
			e = io.EOF
		}
	case transport.CommnadDNS:
		e = s.responseDNS(ctx, session, request)
	case transport.CommnadPing:
		message.SetCommand(request, transport.CommnadSuccess)
		e = session.Send(ctx, request)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "responsePing"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
		}
	case transport.CommandRuntime:
		e = s.responseRuntime(ctx, session)
	default:
		e = fmt.Errorf("not support command [%v]", cmd)
		if ce := logger.Logger.Check(zap.WarnLevel, "Service.callResponse"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	return
}
func (s *Service) responseRuntime(ctx context.Context, session *call.Session) (e error) {
	var response *command.RuntimeResponse
	if s.debug {
		response = &command.RuntimeResponse{
			GOOS:    runtime.GOOS,
			GOARCH:  runtime.GOARCH,
			Version: runtime.Version(),

			Tag:    version.Tag,
			Commit: version.Commit,
			Date:   version.Date,

			Started: s.started.Unix(),

			NumCPU:       int32(runtime.NumCPU()),
			NumGoroutine: int64(runtime.NumGoroutine()),
			NumCgoCall:   runtime.NumCgoCall(),

			Debug:   true,
			Session: s.trace.Session,
			Stream:  s.trace.Stream,
			Proxy:   s.trace.Proxy,
			Request: s.trace.Request,
		}
	} else {
		response = &command.RuntimeResponse{
			GOOS:    runtime.GOOS,
			GOARCH:  runtime.GOARCH,
			Version: runtime.Version(),

			Tag:    version.Tag,
			Commit: version.Commit,
			Date:   version.Date,

			Started: s.started.Unix(),

			NumCPU:       int32(runtime.NumCPU()),
			NumGoroutine: int64(runtime.NumGoroutine()),
			NumCgoCall:   runtime.NumCgoCall(),
		}
	}
	var b []byte
	b, e = proto.Marshal(response)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseRuntime"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	b, e = message.Marshal(transport.CommnadSuccess, b)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseRuntime"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	e = session.Send(ctx, b)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseRuntime"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}
func (s *Service) responseLogin(ctx context.Context, session *call.Session) (e error) {
	msg, e := message.Marshal(transport.CommnadSuccess, nil)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseLogin"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	e = session.Send(ctx, msg)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseLogin"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}
func (s *Service) responseConnect(ctx context.Context, session *call.Session, msg []byte) (e error) {
	addr := string(message.Body(msg))
	if ce := logger.Logger.Check(zap.DebugLevel, "Connect"); ce != nil {
		ce.Write(
			zap.String("addr", addr),
		)
	}
	var dialer net.Dialer
	c, e := dialer.DialContext(ctx, "tcp", addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Dial"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		// 通知 失敗
		s.sendError(ctx, session, e)
		return
	}
	defer c.Close()
	// 通知 成功
	msg, e = message.Marshal(transport.CommnadSuccess, nil)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Marshal"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	e = session.Send(ctx, msg)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Dial Response"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.DebugLevel, "Bridge"); ce != nil {
		ce.Write(
			zap.String("addr", addr),
		)
	}
	// 橋接 網路
	if s.debug {
		utils.Bridge(c, session.RW.(transport.Stream),
			s.net.Buffer,
			time.Second*5, s.net.Bridge,
			&s.trace.Proxy, &s.trace.Request,
		)
	} else {
		utils.Bridge(c, session.RW.(transport.Stream),
			s.net.Buffer,
			time.Second*5, s.net.Bridge,
			nil, nil,
		)
	}
	return
}

func (s *Service) sendError(ctx context.Context, session *call.Session, e error) {
	if e != context.DeadlineExceeded && e != context.Canceled {
		msg, _ := message.MarshalError(transport.CommnadError, e)
		if msg != nil {
			session.Send(ctx, msg)
			time.Sleep(time.Millisecond * 100)
		}
	}
	return
}
func (s *Service) responseDNS(ctx context.Context, session *call.Session, msg []byte) (e error) {
	if !s.dns {
		e = errForwardDNSNotEnable
		s.sendError(ctx, session, e)
		return
	}

	var request command.DNSRequest
	e = proto.Unmarshal(message.Body(msg), &request)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}

	// 創建 dns 客戶端
	client := &dns.Client{}
	if request.TCP {
		client.Net = "tcp"
	}
	// 發送 請求
	q := &dns.Msg{}
	e = q.Unpack(request.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	var in *dns.Msg
	in, _, e = client.ExchangeContext(ctx, q, request.DNS)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	// 返回 響應
	b, e := in.Pack()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	msg, e = message.Marshal(transport.CommnadSuccess, b)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.sendError(ctx, session, e)
		return
	}
	e = session.Send(ctx, msg)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "responseDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}
