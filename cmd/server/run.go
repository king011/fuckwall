package server

import (
	"crypto/sha512"
	"os"

	"gitlab.com/king011/fuckwall/configure"
)

// Run 運行 服務器
func Run(cnf *configure.Server, debug bool) {

	// 創建 服務
	s := Service{
		debug: debug,
		dns:   cnf.DNS,
		net:   &cnf.NET,
	}

	pwd := sha512.Sum512([]byte(cnf.Password))
	if cnf.Quic != nil {
		e := s.InitQuic(cnf, pwd[:])
		if e != nil {
			os.Exit(1)
		}
	}
	if cnf.GRPC != nil {
		e := s.InitGRPC(cnf, pwd[:])
		if e != nil {
			os.Exit(1)
		}
	}

	// 運行 服務
	s.Run()
}
