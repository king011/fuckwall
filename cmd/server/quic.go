package server

import (
	"crypto/tls"
	"log"
	"os"

	"gitlab.com/king011/fuckwall/configure"
	"gitlab.com/king011/fuckwall/logger"
	"gitlab.com/king011/fuckwall/transport/tquic"
	"go.uber.org/zap"
)

// InitQuic 初始化 quic 服務器
func (s *Service) InitQuic(cnf *configure.Server, pwd []byte) (e error) {
	// 加載 x509 證書
	cert, e := tls.LoadX509KeyPair(cnf.Quic.CertFile, cnf.Quic.KeyFile)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "quic init"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}

	// 創建 監聽器
	l, e := tquic.NewListener(pwd,
		cnf.Quic.Addr,
		&tls.Config{
			// 配置 證書
			Certificates: []tls.Certificate{cert},
		},
		nil,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "quic init"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
	if !logger.Logger.OutConsole() {
		log.Println("quic work at", cnf.Quic.Addr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "quic work"); ce != nil {
		ce.Write(
			zap.String("addr", cnf.Quic.Addr),
		)
	}
	s.listeners = append(s.listeners, l)
	return
}
