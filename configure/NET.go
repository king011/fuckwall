package configure

import (
	"time"
)

// NET 路網 配置
type NET struct {
	// 請求超時 時間 (單位:秒)
	// 默認 10 秒
	Request time.Duration
	// 網橋 未活躍 超時 斷線時間  (單位:秒)
	// 默認 5 分鐘
	Bridge time.Duration
	// 每個連接 網路數據包 緩衝區大小
	// 默認 1024 * 32
	Buffer int
}

// Format 格式化 參數
func (n *NET) Format() (e error) {
	if n.Request < 1 {
		n.Request = time.Second * 10
	} else {
		n.Request *= time.Second
	}

	if n.Bridge < 30 {
		n.Bridge = time.Minute * 5
	} else {
		n.Bridge *= time.Second
	}

	if n.Buffer < 1024 {
		n.Buffer = 1024
	}
	return
}
