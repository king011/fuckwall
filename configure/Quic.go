package configure

import (
	"path/filepath"
	"strings"
)

// Quic quic 服務器 配置
type Quic struct {
	// 服務器監聽地址
	Addr string
	// x509 證書路徑
	CertFile string
	KeyFile  string
}

// Format 格式化 參數
func (q *Quic) Format(basePath string) (e error) {
	q.Addr = strings.TrimSpace(q.Addr)
	q.CertFile = strings.TrimSpace(q.CertFile)
	q.KeyFile = strings.TrimSpace(q.KeyFile)
	if q.CertFile == "" {
		q.CertFile = "server.pem"
	}
	if q.KeyFile == "" {
		q.KeyFile = "server.key"
	}

	if filepath.IsAbs(q.CertFile) {
		q.CertFile = filepath.Clean(q.CertFile)
	} else {
		q.CertFile = filepath.Clean(basePath + "/" + q.CertFile)
	}
	if filepath.IsAbs(q.KeyFile) {
		q.KeyFile = filepath.Clean(q.KeyFile)
	} else {
		q.KeyFile = filepath.Clean(basePath + "/" + q.KeyFile)
	}

	return
}
