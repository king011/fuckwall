package configure

import (
	"encoding/json"
	"io/ioutil"
	"strings"

	jsonnet "github.com/google/go-jsonnet"
	logger "gitlab.com/king011/king-go/log/logger.zap"
)

// Client 客戶端 配置
type Client struct {
	// 服務器 設置
	Remote Remote

	// socks5 監聽地址
	// 如果 爲空 則不創建 socks5 代理
	Socks5 string
	// iptables 透明代理 監聽地址
	// 如果 爲空 則不創建 redir 代理
	Redir string

	// DNS 配置 如果爲空 則不進行 dns 轉發
	DNS *DNS

	// 路網 配置
	NET NET

	Logger logger.Options
}

// Format 格式化 參數
func (c *Client) Format() (e error) {
	c.Socks5 = strings.TrimSpace(c.Socks5)
	c.Redir = strings.TrimSpace(c.Redir)

	if c.DNS != nil {
		e = c.DNS.Format()
		if e != nil {
			return
		}
	}
	e = c.Remote.Format()
	if e != nil {
		return
	}

	e = c.NET.Format()
	if e != nil {
		return
	}
	return
}
func (c *Client) String() string {
	if c == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(c, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (c *Client) Load(filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, c)
	if e != nil {
		return
	}
	e = c.Format()
	return
}

// DNS 配置
type DNS struct {
	// 本地 dns 服務器地址
	LAddr string
	// dns 服務器地址
	Server string
}

// Format 格式化 參數
func (d *DNS) Format() (e error) {
	d.LAddr = strings.TrimSpace(d.LAddr)
	d.Server = strings.TrimSpace(d.Server)
	if d.LAddr == "" {
		d.LAddr = "127.0.0.1:10054"
	}
	if d.Server == "" {
		d.Server = "8.8.8.8:53"
	}
	return
}

// Remote 服務器 設置
type Remote struct {
	// 傳輸協議
	Transport string
	// 服務器 地址
	Addr string
	// 密碼
	Password string

	// 是否使用 TLS 安全 傳輸
	// 對於 quic TLS 始終爲 true
	TLS bool

	// 是否 忽略 證書驗證
	SkipVerify bool
}

// Format 格式化 參數
func (r *Remote) Format() (e error) {
	r.Transport = strings.ToLower(strings.TrimSpace(r.Transport))
	r.Addr = strings.TrimSpace(r.Addr)
	if r.Transport == "" {
		r.Transport = "quic"
	}

	return
}
