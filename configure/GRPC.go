package configure

import (
	"path/filepath"
	"strings"
)

// GRPC grpc 服務器 配置
type GRPC struct {
	// 服務器監聽地址
	Addr string
	// x509 證書路徑
	CertFile string
	KeyFile  string
}

// Format 格式化 參數
func (g *GRPC) Format(basePath string) (e error) {
	g.Addr = strings.TrimSpace(g.Addr)
	g.CertFile = strings.TrimSpace(g.CertFile)
	g.KeyFile = strings.TrimSpace(g.KeyFile)

	if g.CertFile != "" {
		if filepath.IsAbs(g.CertFile) {
			g.CertFile = filepath.Clean(g.CertFile)
		} else {
			g.CertFile = filepath.Clean(basePath + "/" + g.CertFile)
		}
	}
	if g.KeyFile != "" {
		if filepath.IsAbs(g.KeyFile) {
			g.KeyFile = filepath.Clean(g.KeyFile)
		} else {
			g.KeyFile = filepath.Clean(basePath + "/" + g.KeyFile)
		}
	}
	return
}

// H2 返回 是否 使用 H2 協議
func (g *GRPC) H2() bool {
	return g.KeyFile != "" && g.CertFile != ""
}
