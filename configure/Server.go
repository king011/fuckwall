package configure

import (
	"encoding/json"
	"io/ioutil"

	jsonnet "github.com/google/go-jsonnet"
	logger "gitlab.com/king011/king-go/log/logger.zap"
)

// Server 服務器 配置
type Server struct {
	// Quic 服務器 配置
	Quic *Quic

	// GRPC 服務器 配置
	GRPC *GRPC

	// 如果爲 true 開啓 dns 轉發
	DNS bool
	// 客戶端 驗證 密碼
	Password string

	// 路網 配置
	NET NET

	// 日誌 設置
	Logger logger.Options
}

// Format 格式化 參數
func (s *Server) Format(basePath string) (e error) {
	if s.GRPC != nil {
		e = s.GRPC.Format(basePath)
		if e != nil {
			return
		}
	}
	if s.Quic != nil {
		e = s.Quic.Format(basePath)
		if e != nil {
			return
		}
	}
	e = s.NET.Format()
	if e != nil {
		return
	}
	return
}
func (s *Server) String() string {
	if s == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(s, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (s *Server) Load(basePath, filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, s)
	if e != nil {
		return
	}
	e = s.Format(basePath)
	return
}
